
const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const morgan = require("morgan");
const DEBUG = require('debug')('API:index');
const fileUpload = require('express-fileupload');
// const zip = require('express-zip');
const compression = require('compression')


const mongo_config = require('./config/DB');
const api_config = require('./config/API');

global.AdminUser = require('./models/AdminUserModel');
global.Token=require('./models/TokenModel');
global.Survey=require('./models/SurveyModel');
global.Language=require('./models/SurveyLanguageModel');

const SurveyController = require( "./controllers/SurveyController.js");

const routes = require('./routes/Route');
const routesAdminUsers = require('./routes/AdminUserRoute');
const routesTokens = require('./routes/TokenRoute');
const routeSurvey = require('./routes/SurveyRoute');
const API = require('./config/API');

mongoose.connect(
    mongo_config.DB,
    mongo_config.options
).then(
    () => {console.log('Database is connected') },
    err => { console.log('Can not connect to the database'+ err)}
);

SurveyController.initSurveyTemplate();


const app = express();
app.use(compression())
app.use(morgan("dev"));
app.use(cors());
app.use(bodyParser.json({limit: "50mb"}));
app.use(fileUpload({ safeFileNames: true,preserveExtension: true  }))
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true, parameterLimit: 50000 }))
routes(app);
routesAdminUsers(app);
routesTokens(app);
routeSurvey(app);
app.listen(api_config.port);

app.use((req, res) => {
    res.status(404).json({ message: "Not found" ,status:"Error"});
});

console.log(`Server started on port ${api_config.port}`);

console.log("URL is : "+api_config.URL);

API.root = __dirname

console.log(`API root ${API.root}`)