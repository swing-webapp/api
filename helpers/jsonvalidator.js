const Ajv = require("ajv")
const addFormats = require("ajv-formats")
const _ = require('lodash');


const prepossurvey={
    $id:'prepossurvey',
    type:"object",
    properties:{
        enable:{type:"boolean"},
        mandatory:{type:"boolean"},
        long:{type:"boolean"},
        _id:{type:"string"},
        id:{type:"string",const: {$data: "1/_id"}},
        short_name:{type:"string"},
        description:{type:"string",nullable:true},
        comments:{type:"string",nullable:true},
        type:{type:"string"},
        type_attribut:{type:"object",nullable:true},
        contents:{
            type:"array",
            items:{
                type:'object',
                properties:{
                    language:{type:"string"},
                    _id:{type:"string"},
                    id:{type:"string",const: {$data: "1/_id"}},
                    html:{type:"object",
                        properties:{
                            inputs:{type:"string"},
                            content:{type:"string"}
                        },
                        required:['inputs','content'],
                        additionalProperties:false
                            }
                },
                additionalProperties:false,

            },
        },
    },
    required:['enable','mandatory','long','_id','short_name','type','contents'],
    additionalProperties:false

}

const contents={
    $id:"contents",
    type:"object",
    properties:{
        _id:{type:"string"},
        id:{type:"string",const: {$data: "1/_id"}},
        language:{type:"string"},
        html:{type:"object",
                        properties:{
                            content:{type:"string"}
                        },
                        required:['content'],
                        additionalProperties:false
                },
        },
    required:['language','html'],
    additionalProperties:false}

const contentsalt ={
    $id:"contentsalt",
    type:"object",
    properties:{
        _id:{type:"string"},
        id:{type:"string",const: {$data: "1/_id"}},
        language:{type:"string"},
        html:{type:"object",
                        properties:{
                            content:{type:"string"},
                            caption:{type:"string"}
                        },
                        required:['content','caption'],
                        additionalProperties:false
                },
        },
    required:['language','html'],
    additionalProperties:false}



const schema = {
  type: "object",
  properties: {
    general:{
        type:"object",
        properties:{
            enable:{type:"boolean"},
            short_name:{type:"string"},
            description:{type:"string",nullable:true},
            comments:{type:"string",nullable:true},
            languages:{type:"array", uniqueItems: true,items: {type: "string"}},
            dates:{type:"object"},
            externalurl:{
                type:"object",
                properties:{
                    enableeentryurl:{type:"boolean"},
                    enableexiturl:{type:"boolean"},
                    exiturl:{type:"string",nullable:true},
                    pointofentry:{type:"string",nullable:true},
                    pointofexit:{type:"string",nullable:true},

                },
                required:['enableeentryurl','enableexiturl'],
                additionalProperties:false,

            },
            poc:{
                type:"object",
                properties:{
                    _id:{type:"string"},
                    id:{type:"string",const: {$data: "1/_id"},},
                    useglobal:{type:"boolean"},
                    firstname:{type:"string"},
                    lastname:{type:"string"},
                    email:{type:"string",format:"email"},
                    address:{type:"object",
                            properties:{
                                street:{type:"string"},
                                zipcode:{type:"number"},
                                city:{type:"string"},
                                country:{type:"string"},
                            },
                        nullable:true,
                        additionalProperties:false,
                        },
                    phoneumber:{type:"number"}
                },
                required:['useglobal'],
                additionalProperties:false,
            },
        },
        required: ['enable','short_name','description','comments',
        'languages','dates','externalurl','poc'],
        additionalProperties:false,
    },
    presurvey:{
        type:"array",
        items:{ $ref: "prepossurvey"}
    },
    postsurvey:{
        type:"array",
        items:{ $ref: "prepossurvey"}
    },
    welcome:{
        type:"array",
        items:{ $ref: "contents"}
    },
    rational:{
        type:"object",
        properties:{
            general:{
                type:"object",
                properties:{
                    learnloop:{type:"boolean"},
                    prop_retry:{type:"boolean"},
                    swing:{type:"boolean"},
                    tradeoff:{type:"boolean"},
                    download_weight:{type:"boolean"},
                    order:{type:'string',enum:['ST','TS']},
                    tradeoff_method:{type:'string',enum:['direct','stepwise']},
                    midval:{
                        type:"object",

                    },
                    conclusiontext:{
                        type:"object",
                        properties:{
                            content:{
                                type:"array",
                                items:{ $ref: "contents"}
                            }
                        },
                        additionalProperties:false,
                        required:["content"],
                    }

                },
                additionalProperties:false,
                required:['learnloop','prop_retry','swing','tradeoff',
                'download_weight','order','tradeoff_method','midval','conclusiontext']

            },
            alternatives:{
                type:"object",
                properties:{
                    content:{
                        type:"array",
                        items:{ $ref: "contents"}
                    },
                    items:{
                        type:'array',
                        items:{
                            type:'object',
                            properties:{
                                _id:{type:"string"},
                                id:{type:"string",const: {$data: "1/_id"}},
                                short_name:{type:"string"},
                                description:{type:"string",nullable:true},
                                comments:{type:"string",nullable:true},
                                enable:{type:"boolean"},
                                elements:{
                                    type:"object",
                                    properties:{
                                        picture:{type:"string",format:"uri"},
                                        contents:{
                                            type:"array",
                                            items:{ $ref: "contentsalt"}
                                        },
                                    },
                                    additionalProperties:false,
                                    required:['picture','contents']
                                }
                                
                            },
                            additionalProperties:false,
                            required:['_id','short_name','enable','elements',]
                        }
                    }
                },
                additionalProperties:false,
                required:['content','items']
            },
            objectives:{
                type:"object",
                properties:{
                    content:{
                        type:"array",
                        items:{ $ref: "contents"}
                    },
                    items:{
                        type:'array',
                        items:{
                            type:'object',
                            properties:{
                                _id:{type:"string"},
                                id:{type:"string",const: {$data: "1/_id"}},
                                short_name:{type:"string"},
                                description:{type:"string",nullable:true},
                                comments:{type:"string",nullable:true},
                                enable:{type:"boolean"},
                                midval_manual_enable:{type:"boolean"},
                                type:{type:"string",enum:['objective','category']},
                                elements:{
                                    type:"object",
                                    properties:{
                                        picture:{type:"string",format:"uri"},
                                        contents:{
                                            type:"array",
                                            items:{ $ref: "contents"}
                                        },
                                        min:{type:"number"},
                                        max:{type:"number"},
                                        step:{type:"number"},
                                        interp:{type:"string",enum:['linear','exp']},
                                        color:{type:"string"},
                                        unit:{type:"object"},
                                        interpC:{type:"number"},

                                    },
                                    additionalProperties:false,
                                    required:['picture','contents']
                                },
                                children:{
                                    type:'array',
                                    nullable:true,
                                    items:{
                                        type:'object',
                                        properties:{
                                            _id:{type:"string"},
                                            id:{type:"string",const: {$data: "1/_id"}},
                                            short_name:{type:"string"},
                                            description:{type:"string",nullable:true},
                                            comments:{type:"string",nullable:true},
                                            enable:{type:"boolean"},
                                            midval_manual_enable:{type:"boolean"},
                                            type:{type:"string",enum:['objective']},
                                            children:{type:"array",maxItems:0},
                                            elements:{
                                                type:"object",
                                                properties:{
                                                    picture:{type:"string",format:"uri"},
                                                    contents:{
                                                        type:"array",
                                                        items:{ $ref: "contents"}
                                                    },
                                                    min:{type:"number"},
                                                    max:{type:"number"},
                                                    step:{type:"number"},
                                                    interp:{type:"string",enum:['linear','exp']},
                                                    color:{type:"string"},
                                                    unit:{type:"object"},
                                                    interpC:{type:"number"},
            
                                                },
                                                additionalProperties:false,
                                                required:['picture','contents','min','max','step','interp',]
                                            },
                                        },
                                    additionalProperties:false,
                                    required:['_id','short_name','enable','elements','type',]
                                    },
                                }
                            },
                            additionalProperties:false,
                            required:['_id','short_name','enable','elements','type',]
                        },
                    }
                }
            },
            predicmat:{
                type:"object",
                additionalProperties: {
                    type: "object",
                    additionalProperties: {type:"number"}
                },
                nullable:true,
            },
        },
        required:['general','alternatives','objectives'],
        additionalProperties:true

    },
    createdAt:{type:"string"},
    updatedAt:{type:"string"},
    status:{type:'boolean'},
    poc:{type:"object"},
    _id:{type:"string"},
    id:{type:"string",const: {$data: "1/_id"}},



  },
  required: ["general",'welcome','rational'],
  additionalProperties: false
}


class ValidateSchema {
    constructor(){
        this.ajv = new Ajv({allErrors: true,strict:true,$data: true,allowUnionTypes:true}).addSchema([prepossurvey,contents,contentsalt])
        addFormats(this.ajv)
        this.comp = this.ajv.compile(schema)
    }

    validate(data){
        const valid = this.comp(data)
        return {valid:valid,errors:this.comp.errors}
    }

}
class ValidateData {
    constructor(){}


    validate(data){

        let errors=[]
        
        if(data.rational.predicmat){
            let pmat = data.rational.predicmat
            let ListAltMcda=Object.keys(pmat);
            let LAM2 = _.uniq(ListAltMcda);

            if(ListAltMcda.length != LAM2.length) {
                errors.push("Alternatives keys are not unique in mcda");
            }


            let ListAltRat=this.getAlt(data);
            let LAR2 = _.uniq(ListAltRat);
            if(ListAltRat.length != LAR2.length) {
                errors.push("Alternatives keys are not unique in rational");
            }

            _.pullAll(LAM2,ListAltRat)

            if(LAM2.length>0){
                errors.push(LAM2.toString() + ' are missing in MCDA');
            }

            _.pullAll(LAR2,ListAltMcda)

            if(LAR2.length>0){
                errors.push(LAR2.toString() + ' are missing in Alternatives');
            }
            let ObjRat = this.getObj(data);
            let ListObjRat = Object.keys(ObjRat);
            let LOR2 = _.uniq(ListObjRat);
            if(ListObjRat.length != LOR2.length) {
                errors.push("Objectives keys are not unique in rational");
            }

            Object.entries(pmat).forEach(alt=>{
                let listobj = Object.keys(alt[1]);
                let lo = _.uniq(listobj);
                if(listobj.length != lo.length) {
                    errors.push("mcda key: "+alt + " Objectives keys are not unique in mcda");
                }
                _.pullAll(lo,listobj)

                if(lo.length>0){
                    errors.push("mcda key: "+alt +" "+lo.toString() + ' are missing in MCDA');
                }
                _.pullAll(_.clone(ListObjRat),listobj)
    
                if(LAR2.length>0){
                    errors.push("mcda key: "+alt +" "+LAR2.toString() + ' are missing in Objective');
                }
                Object.entries(alt[1]).forEach(obj => {
                    if(ListObjRat.includes(obj[0])){
                        const tobj=ObjRat[obj[0]];
                        if(tobj.elements && tobj.elements.min && tobj.elements.max ){
                            if ((tobj.elements.min < tobj.elements.max && (obj[1]<tobj.elements.min || obj[1]>tobj.elements.max ))
                            || (tobj.elements.min > tobj.elements.max && (obj[1]>tobj.elements.min || obj[1]<tobj.elements.max ))){
                                errors.push("mcda key: "+alt[0] +" obj: "+obj[0]+ ' value is out of range');
                            }
                        }
                    }
                })
            })

        }
        return {valid:errors.length==0,errors:errors}
    }

     getAlt(data){
        let altlist = []
        data.rational.alternatives.items.forEach(
            (alt) => altlist.push(alt.short_name))

        return altlist
    }
     getObj(data){
        let objectives = {};
        data.rational.objectives.items.forEach((key) => {
            if(key.type==='objective' && key.short_name) objectives[key.short_name]=key;
            if(key.children) {
                key.children.forEach((key) => {
                    if(key.type==='objective' && key.short_name) objectives[key.short_name]=key;
                })
            }})
        return objectives
    }

}
module.exports = {ValidateSchema,ValidateData}