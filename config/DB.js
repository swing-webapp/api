const host = process.env.mongo_host || "localhost:27017";

module.exports = {
    DB: `mongodb://${host}/swingeawag`,
    options :
        { useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify:false,
            useCreateIndex: true,
        }
};
