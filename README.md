API Eawag Survey
======


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm start
```

### Compiles and minifies for production
```
npm start
```

## Files

    ./LICENSE : MIT License
    ./README.md
    ./config : directory with config files
        ./config/API.js : Concern the configuration of the API
        ./config/DB.js : Concern the configuration of the DB connection (mongo)
    ./controllers : Controlers are functions used in the API
        ./controllers/SurveyController.js : Functions used for surveys
        ./controllers/TokenController.js: Functions used for token
        ./controllers/UserController.js : Functions used for users
    ./filename.txt : this file list generated using tree -ifI node_modules > filename.txt
    ./helpers : function used for adding functionnalities 
        ./helpers/errors.js : not used
    ./index.js : main file, used to initiate the API server
    ./middleware : function used between functions
        ./middleware/auth.js : manages authentification headers
        ./middleware/errorHandler.js : not used
    ./models : mongo database model definition
        ./models/AdminUserModel.js : model for users
        ./models/SurveyLanguageModel.js : model for languages (template and translation)
        ./models/SurveyModel.js : models for surveys
        ./models/TokenModel.js : model for token
    ./package-lock.json : npm file for managing packages
    ./package.json : npm list of command and packages installed for this project
    ./public : external file needed or generated (for ex uploaded)
        ./public/template
            ./public/template/LanguageDef.json : template variable file definition (loaded if corresponding database is empty)
            ./public/template/LanguageTrans.json translation variable file definition (loaded if corresponding database is empty)
        ./public/uploads : not used
    ./routes : contains the different possible routes for the API
        ./routes/AdminUserRoute.js : route for users
        ./routes/Route.js : generic route
        ./routes/SurveyRoute.js : route for survey
        ./routes/TokenRoute.js : route for token
    ./startup.sh : server starting script

    9 directories, 25 files
