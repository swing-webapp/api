const jwt = require("jsonwebtoken");
api_config = require('../config/API');

module.exports =(admin = false)=> (req, res, next) => {
    try {
        const token = req.headers.authorization.replace("Bearer ", "");
        req.userData = jwt.verify(token, api_config.token);
        if (admin && admin !== req.userData.admin){
            return res.status(401).json({
                message: "Authentication Failed",
                Status : "Error"
            });
        } else {
            next();
        }
    } catch (err) {
        return res.status(401).json({
            message: "Authentication Failed",
            Status : "Error"
        });
    }
};
