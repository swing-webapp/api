#!/bin/sh
if [ "$NODE_ENV" = "production" ]; then
  node index.js;
else
  export NODE_ENV="development";
  nodemon index.js;
fi
