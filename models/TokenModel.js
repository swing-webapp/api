api_config = require('../config/API');

const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");

const { Schema } = mongoose;

const TokenSchema =  new Schema({
    token: {
        type: String,
        required:true
    },
    survey_id: {
        type: String,
        required:true
    },
    comments: {
        type: String
    },
    enable: {
        type: Boolean,
        required: true,
        default:true
    },
    test: {
        type: Boolean,
        required: false,
        default:false
    },
    access: {
        type: Boolean,
        required: true,
        default:false
    },
    status: {
        type: String,
        enum:['new','connected','finished'],
        required:true
    },
    answers:{
        type : Object,
    },
    submitedAt:{
        type: Date
    }
},{ timestamps: true });

TokenSchema.methods.generateAuthToken= async function(extrafields) {
    const user = this;
    return jwt.sign({
            id: user._id,
            token: user.token,
            survey_id: user.survey_id,
            status: user.status,
            access:user.access,
            admin: false,
            test:user.test,
            answers:user.answers,
            ...extrafields
        },
        api_config.token, {expiresIn: "1d",});
};

TokenSchema.statics.tokenById = async (id) => {
    let token = await Token.findById(id)
    if(token){
        return token.token;
    }else{
        return "unknown"
    }
};


TokenSchema.methods.toJSON = function () {
    const token = this;
    const Obj = token.toObject();
    delete Obj.__v;
    const id= Obj._id;
    delete Obj._id;
    Obj.id = id;
    return Obj;
};


TokenSchema.statics.checkExistingField = async (field, value) => {
    return await Token.findOne({[`${field}`]: value});
};

module.exports = mongoose.model("Token", TokenSchema);
