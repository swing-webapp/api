api_config = require('../config/API');

const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { Schema } = mongoose;
const validator = require( "validator");

const AdminUserSchema =  new Schema({
    firstname: {
        type: String,
        required:true
    },
    lastname: {
        type: String,
        required:true
    },
    admin: {
        type:Boolean,
        default: true
    },
    username: {
        type: String,
        validate: [validator.isEmail, "Please provide a valid email address"],
        required: [true, 'Username is required'],
        unique: true
    },
    password: {
        type: String,
        required: "Please include your password"
    },
    comments: {
        type: String
    },
    enable: {
        type: Boolean,
        required: true,
        default:true
    }
},{ timestamps: true });

//this method will hash the password before saving the user model
AdminUserSchema.pre("save", async function(next) {
    const user = this;
    if (user.isModified("password")) {
        user.password = await bcrypt.hash(user.password, api_config.bcrypt_salt);
    }
    next();
});


//this method generates an auth token for the user
AdminUserSchema.methods.generateAuthToken= async function() {
    const user = this;
    return jwt.sign({
            id: user._id,
            username: user.username,
            firstname: user.firstname,
            lastname: user.lastname,
            comments: user.comments,
            admin: true
        },
        api_config.token, {expiresIn: "1d",});
};


AdminUserSchema.methods.comparePassword = async function (password) {
    return await bcrypt.compare(password, this.password);
};

AdminUserSchema.statics.nameById = async function(id) {
    let user = await this.findById(id)
    if(user){
        return user.username;
    }else{
        return "unknown"
    }
};


AdminUserSchema.methods.toJSON = function () {
    const user = this;
    const userObj = user.toObject();
    delete userObj.password;
    delete userObj.__v;
    const id= userObj._id;
    delete userObj._id;
    userObj.id = id;
    return userObj;
};


AdminUserSchema.statics.checkExistingField = async function(field, value)  {
    return await this.findOne({[`${field}`]: value});
};

module.exports = mongoose.model("AdminUser", AdminUserSchema);
