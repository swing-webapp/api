const mongoose = require("mongoose");
const { Schema } = mongoose;

const SurveyLanguageDefSchema =  new Schema({
    field:{
        type:String,
        required:true,
    },
    description:{
        type:String,
    },
    type:{
        type:String,
        default: "String",
        enum:['String','html'],
        required:true,
    },

},{ timestamps: true });
SurveyLanguageDefSchema.methods.toJSON = function () {
    const sld = this;
    const Obj = sld.toObject();
    delete Obj.__v;
    const id= Obj._id;
    delete Obj._id;
    Obj.id = id;
    return Obj;
};
SurveyLanguageDefSchema.statics.checkExistingField = async function(field, value)  {
    return await this.findOne({[`${field}`]: value});
};
module.exports = mongoose.model("SurveyLanguageDef", SurveyLanguageDefSchema);

const SurveyLanguageTransSchema =  new Schema({
    language:{
        type:String,
        required:true,
    },
    flag:{
        type:String,
        required:true,
    },
    fields:[{
        field: {
            type: String,
            required: true,
        },
        text:{
            type: String,
            required: false,
        },

    }]

},{ timestamps: true });

SurveyLanguageTransSchema.virtual('status').get(function() {
    return "TODO";
});

SurveyLanguageTransSchema.statics.checkExistingField = async function(field, value) {
    return await this.findOne({[`${field}`]: value});
};
SurveyLanguageTransSchema.methods.toJSON = function () {
    const slt = this;
    const Obj = slt.toObject({virtuals: true});
    delete Obj.__v;
    const id= Obj._id;
    delete Obj._id;
    Obj.id = id;
    return Obj;
};

module.exports = mongoose.model("SurveyLanguageTrans", SurveyLanguageTransSchema);
