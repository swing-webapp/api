api_config = require('../config/API');
const mongoose = require("mongoose");
const { Schema } = mongoose;
const validator = require( "validator");


const textinputs = new Schema({
            language:String,
            html:{
                content:String,
                inputs:String, }
                ,});

const text = new Schema({
            language:String,
            html:{
                content:String,
            },
});

const textcaption = new Schema({
        language:String,
        html:{
            caption:String,
            content:String,
        },});

const altschema = new Schema({
        _id:mongoose.ObjectId,
        short_name: {
            type: String,
        },
        description: {
            type: String,
        },
        comments: {
            type: String,
        },
        enable: {
            type: Boolean,
            default: true
        },
        elements:{
            picture: String,
            contents:[textcaption],
            },
    });
const objschema = new Schema({
        _id:mongoose.ObjectId,
        short_name: {
            type: String,
        },
        description: {
            type: String,
        },
        comments: {
            type: String,
        },
        enable: {
            type: Boolean,
            default: true
        },
        midval_manual_enable:{
            type: Boolean,
            default: false
        },
        children:Array,
        type:{
            type: String,
            enum:['objective','category'],
            required:true
        },
        elements:Object,
    });

const POCSchema = new Schema({
    firstname: {
        type: String,
    },
    lastname: {
        type: String,
    },
    useglobal: {
        type:Boolean,
        required:true,
        default: false
    },
    email: {
        type: String,
    },
    address : {
        street:String,
        zipcode : Number,
        city : String,
        country : String
    },
    phonenumber:Number,

})

const SurveyGlobalOptSSchema = new Schema({
    _id:Number,
    poc: POCSchema,});

SurveyGlobalOptSSchema.statics.checkExistingField = async function(field, value) {
    return await this.findOne({[`${field}`]: value});
};
module.exports = mongoose.model("SurveyGlobalOptions", SurveyGlobalOptSSchema);

const PrePostSurveySchema = new Schema({
    _id:mongoose.ObjectId,
    short_name: {
        type: String,
        required: true,
    },
    description: {
        type: String,
    },
    comments: {
        type: String,
    },
    enable: {
        type: Boolean,
        required: true,
        default: true
    },
    mandatory:{
        type: Boolean,
        required: true,
        default: false
    },
    long:{
        type: Boolean,
        required: true,
        default: false
    },
    type : {
        type:String,
        required: true
    },
    type_attribut:Object,
    contents: [textinputs],

    });

const SurveySchema =  new Schema({
    general:{
        short_name: {
            type: String,
            required: true,
            unique:true,
        },
        description: {
            type: String,
        },
        comments: {
            type: String,
        },
        enable: {
            type: Boolean,
            required: true,
            default: true
        },
        poc: POCSchema,
        dates:{
            date_start:{
                type:Date
            },
            date_stop:{
                type:Date
            },
            date_start_enable:Boolean,
            date_stop_enable:Boolean,
        },
        languages:{
            type:Array
        },
        externalurl:{
            enableeentryurl:Boolean,
            pointofentry:{
                type : String,
                default: "rational"},
            enableexiturl:Boolean,
            pointofexit:{
                type : String,
                default: "rational"},
            exiturl:String,
        }
    },
    welcome:
        [{
            language:String,
            html:{
                content:String
            },
            movie:{
                url:String
            }
        }],
      presurvey:[PrePostSurveySchema],
      postsurvey:[PrePostSurveySchema],
    rational:{
        general: {
            learnloop:{
                type :Boolean,
                required: true,
                default: true,
            },
            prop_retry:{
                type :Boolean,
                default: false,
            },
            swing:{
                type :Boolean,
                required: true,
                default: true,
            },
            tradeoff:{
                type :Boolean,
                required: true,
                default: false,
            },
            tradeoff_method:{
                type: String,
                enum:['direct','stepwise'],
                required:true
            },
            
            midval:{
                general:{
                    type :String,
                    enum:['off','auto','manual'],
                    required: true,
                    default: 'off',
                },
                auto_threshold:{
                    type: Number,
                    default : 0.6,
                }
                
            },

            order:{
                type :String,
                enum:['ST','TS'],
                default: 'ST',
            },
            download_weight:{
                type :Boolean,
                required: true,
                default: false,
            },
            conclusiontext:{
                content:[text],
            }

        },
        alternatives:{
            content:[text],
            items:[altschema]
        },
        objectives: {
            content:[text],
            items:[objschema],
        },
        predicmat:Object,
    },
},{ timestamps: true });

SurveySchema.statics.checkExistingField = async function(field, value) {
    return await this.findOne({[`${field}`]: value});
};
SurveySchema.statics.isAccessToken = async function(survey_id){
    let survey
    try {
         survey = await this.findById(survey_id);
    } catch {
        return false
    }
    if (survey){
        return survey.general.externalurl.enableeentryurl
    } else {
        return false
    }
};
SurveySchema.virtual('status').get(function() {
    const dates = this.general.dates;
    let check = true
    if(dates.date_start && dates.date_start_enable && dates.date_start.valueOf() > new Date().valueOf()){
        check = false
    } else if(dates.date_stop && dates.date_stop_enable && dates.date_stop.valueOf() < new Date().valueOf()){
        check = false
    }

    return check;
});
SurveySchema.virtual('poc').get(async function() {
    const GlobalOptions = mongoose.model('SurveyGlobalOptions');
    let poc = this.general.poc
    if (this.general.poc.useglobal){
        const opt = await GlobalOptions.findById(0,{poc:1});
        if(opt&&opt.poc){
            poc = opt.poc;
        } else {
            poc = null
        }

    }
    return poc
});
SurveySchema.methods.toJSON = function () {
    const survey = this;
    const Obj = survey.toObject({virtuals:true});
    delete Obj.__v;
    const id= Obj._id;
    delete Obj._id;
    Obj.id = id;
    return Obj;
};

module.exports = mongoose.model("Survey", SurveySchema);



