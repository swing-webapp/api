const mongoose = require('mongoose');
const crypto = require('crypto')
const _ = require('lodash');
const fs = require("fs");
const mime = require('mime-types');
const path = require('path');
const AdmZip = require("adm-zip");
const ncp = require('ncp').ncp;

const API = require("../config/API");

const  { ValidateSchema,ValidateData } =require( '../helpers/jsonvalidator');

const VSchema = new ValidateSchema();
const Vdata = new ValidateData();


const Survey =  mongoose.model('Survey');
const Token =  mongoose.model('Token');
const LanguageDef = mongoose.model('SurveyLanguageDef');
const LanguageTrans = mongoose.model('SurveyLanguageTrans');
const GlobalOptions = mongoose.model('SurveyGlobalOptions');

function randomValueHex(len) {
    return crypto
        .randomBytes(Math.ceil(len / 2))
        .toString('hex') // convert to hexadecimal format
        .slice(0, len) // return required number of characters
}

async function  json2mango(datajson,mongo){
    for (let  i in datajson){
        datajson[i]['_id'] = new mongoose.Types.ObjectId();
    }
    await mongo.create(datajson);
}

exports.dropSurvey = async (req, res) => {
    try {
        // await Survey.collection.drop();
        await Survey.syncIndexes();
        return res.status(201).json({
            message: "Survey drop",
            status: "OK"
        });
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }

}


exports.initSurveyTemplate = async()=>{
    if(await LanguageTrans.countDocuments() === 0){

        let zip = new AdmZip(path.join(API.root,API.template_dir,'LanguageTrans.zip'));
        const jinput = JSON.parse(zip.readAsText('LanguageTrans.json'));
        await json2mango(jinput, LanguageTrans)
        zip.deleteFile('LanguageTrans.json');
        const fpath = path.join(API.uploads_imgdir,'langtrans');
        if(!fs.existsSync(fpath)) fs.mkdirSync(fpath);
        zip.extractAllTo(fpath,true);
            console.log("LanguageTrans initialized")
    }
    if(await LanguageDef.countDocuments() === 0){
        fs.readFile(path.join(API.root,API.template_dir,'LanguageDef.json'), 'utf-8', async (err, data) => {
            if (err) {
                throw err;
            }
            await json2mango(JSON.parse(data.toString()), LanguageDef)

        })
        console.log("LanguageDef initialized")
    }
};

exports.createNewSurvey = async (req, res) => {
    try {
        req.body.general.short_name=req.body.general.short_name.toUpperCase().replace(/ /g, '');
        const checkSN = await Survey.checkExistingField('general.short_name', req.body.general.short_name);
        if (checkSN) {
            return res.status(201).json({
                message: "short name already used",
                status: "Error"
            });
        }

        const survey = new Survey(req.body);
        const newsurvey = await survey.save();

        const sid=newsurvey._id.toString();
        const fpath = path.join(API.uploads_imgdir,sid);
        if(!fs.existsSync(fpath)) fs.mkdirSync(fpath);

        const testtoken = new Token(
            {
                token:randomValueHex(10),
                survey_id:newsurvey._id,
                test:true,
                status:'new',
                comments:'Automatically created by API'
            }
        );
        testtoken.save();
        res.status(201).json({survey:newsurvey,message:"Survey created",status: "OK"});
    } catch (err) {
        res.status(400).json({ message: err,status:"Error" });
    }
};
exports.list_all_surveys = async (req, res) => {
    try {
        const proj={
            general:1,
            status:1,
            createdAt:1,
            updatedAt:1,
        };
        Survey.find({},proj).
        exec(async function (err, surveys) {
            await res.status(201).json({surveys:surveys,message:"",status:"OK"});
        });
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};

exports.get_a_survey = async (req, res) => {
    try {
        const survey = await Survey.findById(req.params.id);
        await res.status(201).json({survey:survey,message:"",status:"OK"});
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};
exports.disable_a_survey = async (req, res) => {
    Survey.findByIdAndUpdate(req.params.id,    {
        "$set": {
            "general.enable": false
        }},{},(err) => {
            if (err) {res.send(err);}
            else{res.json({ id: req.params.id,message:"Survey disabled",status: "OK"});}
        }
    );
};
exports.enable_a_survey = async (req, res) => {
    Survey.findByIdAndUpdate(req.params.id,    {
        "$set": {
            "general.enable": true
        }},{},(err) => {
            if (err) {res.send(err);}
            else{res.json({ id: req.params.id,message:"Survey enabled",status: "OK"});}
        }
    );
};


function FindUrlinHtml(htmlstr,id,langtrans=false){
    const imgRegExp = /(?:<img\s+src="(.*?)"[^.]*?\/?>|&lt;img\s+src="(.*?)"[^.]*?\/?&gt;)/g;
    let urls = [];
    let img;
    while ((img = imgRegExp.exec(htmlstr))) {
        let url
        if((!langtrans) && (url = ClearImgUrL(img[1],id))){
                urls.push(url);
            } else if (url = ClearImgUrLangTrans(img[1],id)){
                urls.push(url);
            }
            
          }
    return urls
}
function ClearImgUrL(url,id){
    const urlRegexp = new RegExp('survey\/'+id+'\/picture\/(.*)');
    url = urlRegexp.exec(url);
    if(url && url[1]){
        return url[1]
     } else {
        return
    }
}
function ClearImgUrLangTrans(url,id){
    const urlRegexp = new RegExp('langtrans\/picture\/(.*)');
    url = urlRegexp.exec(url);
    if(url && url[1]){
        return url[1]
     } else {
        return
    }
}
function getAllFiles(id, dirPath, arrayOfFiles) {
    if (!fs.existsSync(dirPath)) return
    files = fs.readdirSync(dirPath)
    const pathRegexp = new RegExp( path.join(API.uploads_imgdir,id)+'\/(.*)');
    arrayOfFiles = arrayOfFiles || []

    files.forEach(function(file) {
      if (fs.statSync(dirPath + "/" + file).isDirectory()) {
        arrayOfFiles = getAllFiles(id,dirPath + "/" + file, arrayOfFiles)
      } else {
        let fpath = path.join(dirPath,file)
        fpath = pathRegexp.exec(fpath);
        if(fpath && fpath[1]){
            arrayOfFiles.push( fpath[1])
        }
    }
    })
  
    return arrayOfFiles
  }

 function  getImgpath(surveyid){
    return getAllFiles(surveyid,path.join(API.uploads_imgdir,surveyid))
 }
 function  getImgpathLangTrans(lang){
    return getAllFiles('langtrans',path.join(API.uploads_imgdir,'langtrans',lang))
 }
function getImgUrl(survey){
    const varcheck =['welcome','rational.general.conclusiontext.content','rational.alternatives.content','rational.objectives.content',
    'rational.alternatives.items','rational.objectives.items','presurvey','postsurvey'];
    const sid = survey.id;

    let urls=[];
    varcheck.forEach(objn =>{
        const tmp = _.get(survey,objn)
        if(tmp){
            tmp.forEach(el => {
                if(el.html && el.html.content){
                    urls.push(...FindUrlinHtml(el.html.content,sid))
                } else if (el.elements && el.elements.contents) { // for alternatives treatment
                    el.elements.contents.forEach(ell=>{
                        if(ell.html && ell.html.content){
                            urls.push(...FindUrlinHtml(ell.html.content,sid));
                            }
                    })
                    let url
                    if(url = ClearImgUrL(el.elements.picture,sid))
                        {urls.push(url);}
                
                    if(el.children ){
                        //objectives
                        el.children.forEach(ell=>{
                            ell.elements.contents.forEach(elll=>{
                                urls.push(...FindUrlinHtml(elll.html.content,sid))
                            })
                            let url
                            if(url = ClearImgUrL(ell.elements.picture,sid))
                                {urls.push(url);}
                        })
                    }
                }else if (el.contents){ //pre and post surveys
                    el.contents.forEach(ell=>{
         
                    if(ell.html && ell.html.inputs){
                        urls.push(...FindUrlinHtml(ell.html.inputs,sid))
                    }
                    if(ell.html && ell.html.content){
                        urls.push(...FindUrlinHtml(ell.html.content,sid))
                    }

                })
            }


            })
        }
        
   })
   return urls
}

exports.update_a_survey = async (req, res) => {
    
    const sid = req.body.id;
    
   const urls = getImgUrl(req.body);
   const listfile = getImgpath(sid);
   const file2rm = _.difference(listfile,urls);
   file2rm.forEach(fpath => {
    fs.rmSync(path.join(API.uploads_imgdir,sid,fpath));
   })
   const filemissing = _.difference(urls,listfile);

   const validfiles={removed:file2rm.length,missing:filemissing.length}

   const vs = VSchema.validate(req.body)
   const vd = Vdata.validate(req.body)


    Survey.findByIdAndUpdate(req.params.id,req.body,{},(err) => {
        if (err) {res.send(err);}
        else{res.json({ id: req.params.id,message:"Survey modified",status: "OK",validation:{files:validfiles,schema:vs,data:vd}});}
    }
    );
};




exports.delete_a_survey = async (req, res) => {
    try {
        const fpath = path.join(API.root,API.uploads_imgdir,req.params.id);
        if(fs.existsSync(fpath)){
                fs.rmSync(fpath,  {recursive: true});
            }
        await Survey.deleteOne({ _id: req.params.id});
        await Token.remove({survey_id:req.params.id});
        res.json({
            id: req.params.id,
            message: 'Survey successfully deleted',
            status :"OK"
        });
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};

function rImgUrl(txt,file,newurl){
    const urlRegexp = new RegExp('http.*\/picture\/'+file);
    return txt.replace(urlRegexp,newurl)
};

function  modify_elements_contents(obj,listfile,sid){
    obj.forEach(ell=>{
        if(ell.html && ell.html.content){
            listfile.forEach(furl=>{
                let newurl = `${api_config.URL}surveys/survey/${sid}/picture/${furl}`;
                ell.html.content=rImgUrl(ell.html.content,furl,newurl);
            })
        }
    })
    return obj
}
function modify_picture(obj,listfile,sid){
    const parts = obj.split('/').slice(-2).join('/');
    const furl = listfile.find(element => element.endsWith(parts));
    const newurl = `${api_config.URL}surveys/survey/${sid}/picture/${furl}`;
    return newurl
}
function replaceImgUrl(listfile,survey){
    const varcheck =['welcome','rational.general.conclusiontext.content','rational.alternatives.content','rational.objectives.content',
    'rational.alternatives.items','rational.objectives.items','presurvey','postsurvey'];

    const sid = survey._id.toString();;

    varcheck.forEach(objn =>{
        const tmp = _.get(survey,objn)
        if(tmp){
            tmp.forEach(el => {
                if(el.html && el.html.content){ 
                    listfile.forEach(furl=>{
                        let newurl = `${api_config.URL}surveys/survey/${sid}/picture/${furl}`
                        el.html.content=rImgUrl(el.html.content,furl,newurl);
                    })

                } else if (el.elements && el.elements.contents) { // for alternatives treatment
                    el.elements.contents.forEach(ell=>{
                        if(ell.html && ell.html.content){
                            listfile.forEach(furl=>{
                                let newurl = `${api_config.URL}surveys/survey/${sid}/picture/${furl}`
                                ell.html.content=rImgUrl(ell.html.content,furl,newurl);
                            });
                        }     })
                    if(el.elements.picture){
                        el.elements.picture=modify_picture(el.elements.picture,listfile,sid);
                    }
                    if(el.children ){
                        //objectives
                        el.children = el.children.map(child => ({
                            ...child,
                            elements: {
                                ...child['elements'],
                                contents: modify_elements_contents(child['elements']['contents'],listfile,sid),
                                picture  : modify_picture(child['elements']['picture'],listfile,sid),
                              },
                          }));

                    }
               
                } else if (el.contents){ //pre and post surveys
                    el.contents.forEach(ell=>{
         
                        if(ell.html && ell.html.inputs){
                            listfile.forEach(furl=>{
                                let newurl = `${api_config.URL}surveys/survey/${sid}/picture/${furl}`
                                ell.html.inputs=rImgUrl(ell.html.inputs,furl,newurl);
                            })
                    }
                    if(ell.html && ell.html.content){
                        listfile.forEach(furl=>{
                            let newurl = `${api_config.URL}surveys/survey/${sid}/picture/${furl}`
                            ell.html.content=rImgUrl(ell.html.content,furl,newurl);
                        })
                    }
                })

                }
            
            })
        }
    })
    return survey
};


exports.upload_survey = async (req,res) => {
   try{
        if (!req.files || Object.keys(req.files).length === 0) {
            return res.status(400).json({ message: "No file provided",status:"Error"});
        }
        let zip = new AdmZip(req.files.upload.data);

        const survey = JSON.parse(zip.readAsText('survey.json'))
        delete survey._id
        if(req.body.newid){
            survey.general.short_name=req.body.newid;
        }
        survey.general.short_name=survey.general.short_name.toUpperCase().replace(/ /g, '');
        const checkSN = await Survey.checkExistingField('general.short_name', survey.general.short_name);
        if (checkSN) {
            return res.status(201).json({
                message: "short name already used",
                status: "Error"
            });
        }

        const dbsurvey = new Survey(survey);
        let newsurvey = await dbsurvey.save();
        const testtoken = new Token(
            {
                token:randomValueHex(10),
                survey_id:newsurvey._id,
                test:true,
                status:'new',
                comments:'Automatically created by API'
            }
        );
        testtoken.save();

        // copy img

        zip.deleteFile('survey.json');
        const sid=newsurvey._id.toString();
        const fpath = path.join(API.uploads_imgdir,sid);
        if(!fs.existsSync(fpath)) fs.mkdirSync(fpath);
        zip.extractAllTo(fpath,true);

        const listfile = getImgpath(sid);
        newsurvey = replaceImgUrl(listfile,newsurvey)
        let newsurvey2 = await newsurvey.save();

        
        const urls = getImgUrl(newsurvey2);

     
        const file2rm = _.difference(listfile,urls);
        file2rm.forEach(fpath => {
         fs.rmSync(path.join(API.uploads_imgdir,sid,fpath));
        })
        const filemissing = _.difference(urls,listfile);
     
        const validfiles={removed:file2rm.length,missing:filemissing.length}

        const vs = VSchema.validate(JSON.parse(JSON.stringify(newsurvey2)))
        const vd = Vdata.validate(JSON.parse(JSON.stringify(newsurvey2)))
     
        res.status(201).json({survey:newsurvey2,message:"Survey created",status: "OK",validation:{files:validfiles,schema:vs,data:vd}});

  } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
   }
};


exports.clone_survey = async (req,res) => {
   try{
    if(!req.body.srcsn || !req.body.newsn){
        return res.status(201).json({
            message: "input missing",
            status: "Error"
        });
    }
    req.body.newsn=req.body.newsn.toUpperCase().replace(/ /g, '');
    const checkSN = await Survey.checkExistingField('general.short_name', req.body.newsn);
    if (checkSN) {
        return res.status(201).json({
            message: "Destination short name already used",
            status: "Error"
        });
    }

    const survey = await Survey.findOne({'general.short_name':req.body.srcsn.toUpperCase().replace(/ /g, '')} );
    if (!survey) {
        return res.status(201).json({
            message: "Cannot find source",
            status: "Error"
        });
    }

    const survey2 = JSON.parse(JSON.stringify(survey))
    delete survey2._id ;

    dbsurvey = new Survey(_.cloneDeep(survey2));
    dbsurvey.general.short_name = req.body.newsn;
    
    let newsurvey = await dbsurvey.save();
    const testtoken = new Token(
        {
            token:randomValueHex(10),
            survey_id:newsurvey._id,
            test:true,
            status:'new',
            comments:'Automatically created by API'
        }
    );
    testtoken.save();



        // copy img
    const sidsrc =survey._id.toString();
    const fpathsrc = path.join(API.uploads_imgdir,sidsrc);

    const sidnew=newsurvey._id.toString();
    const fpathnew = path.join(API.uploads_imgdir,sidnew);

    if(!fs.existsSync(fpathnew)) fs.mkdirSync(fpathnew);
    await ncp(fpathsrc, fpathnew);

    const listfile = getImgpath(sidsrc);
    newsurvey = replaceImgUrl(listfile,newsurvey)

    let newsurvey2 = await newsurvey.save();

    res.status(201).json({survey:newsurvey2,message:"Survey created",status: "OK"});


 } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
   }
};


exports.download_survey = async (req,res) =>{
   try{
        Survey.findById(req.params.id).
        exec(async function (err, survey) {
            var zip = new AdmZip();
            zip.addFile("survey.json", Buffer.from(JSON.stringify(survey), "utf8"));
            const fpath = path.join(API.root,API.uploads_imgdir,survey.id)
            if(fs.existsSync(fpath)){
                zip.addLocalFolder(fpath);        
            }
            res.set("Content-Type", "application/octet-stream");
            res.set("Content-Disposition", "attachment;filename="+survey.general.short_name+".zip");
            await res.send(zip.toBuffer());
        });
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};

// language definition
exports.createNewLangdef = async (req, res) => {
   try {
        req.body.field=req.body.field.toUpperCase().replace(/ /g, '');;
        const checkfield = await LanguageDef.checkExistingField('field', req.body.field);
        if (checkfield) {
            return res.status(201).json({
                message: 'field already defined',
                status: "Error"
            });
        }

        const ld = new LanguageDef({
            field: req.body.field,
            description: req.body.description,
            type: req.body.type
        });
        const newld = await ld.save();
        res.status(201).json({langdef:newld,message:"Language Definition created",status: "OK"});
    } catch (err) {
        res.status(400).json({ message: err,status:"Error" });
    }
};

exports.update_a_langdef = async (req, res) => {
    try {
        const ld = await LanguageDef.findOne({ _id: req.params.id });


        for (key in req.body) {
            ld[key] = req.body[key];
        }
        await ld.save();
        res.json({ id: req.params.id,message:"Language Definition modified",status: "OK"});
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};

exports.list_all_langdef = async (req, res) => {
    try{
        LanguageDef.find().
        exec(async function (err, langdefs) {
            await res.status(201).json({langdefs:langdefs,message:"",status:"OK"});
        });
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};

exports.save_langdef = async (req,res) =>{
    try{
        LanguageDef.find().
        exec(async function (err, langdefs) {
            fs.writeFileSync(path.join(API.root,API.template_dir,'LanguageDef.json'), JSON.stringify(langdefs,null,4));
            await res.status(201).json({message:"Configuration file saved",status:"OK"});
        });
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};
exports.download_langdef = async (req,res) =>{
    try{
        LanguageDef.find().
        exec(async function (err, langdefs) {
            res.set("Content-Type", "application/octet-stream");
            res.set("Content-Disposition", "attachment;filename=LanguageDef.json");
            res.send(JSON.stringify(langdefs,null,4));
        });
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};

exports.upload_langdef = async (req,res) => {
    try{
        if (!req.files || Object.keys(req.files).length === 0) {
            return res.status(400).json({ message: "No file provided",status:"Error"});
        }
        await LanguageDef.deleteMany();
        await json2mango(JSON.parse(req.files.templatefile.data.toString()), LanguageDef)
        await res.status(201).json({message:"Configuration file uploaded",status:"OK"});
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};


exports.load_langdef= async (req,res) =>{
    try{
        await LanguageDef.deleteMany();
        fs.readFile(path.join(API.root,API.template_dir,'LanguageDef.json'), 'utf-8', async (err, data) => {
            if (err) {
                throw err;
            }
            await json2mango(JSON.parse(data.toString()), LanguageDef)

        })
        await res.status(201).json({message:"Configuration file loaded",status:"OK"});
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};



exports.delete_a_langdef = async (req, res) => {
    try {
        await LanguageDef.deleteOne({ _id: req.params.id});
        res.json({
            id: req.params.id,
            message: 'Language Definition successfully deleted',
            status :"OK"
        });
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};

//language translation

exports.createNewLangtrans = async (req, res) => {
    try {
        req.body.language=req.body.language.toUpperCase().replace(/ /g, '');;
        const checkfield = await LanguageTrans.checkExistingField('language', req.body.language);
        if (checkfield) {
            return res.status(201).json({
                message: 'Language already defined',
                status: "Error"
            });
        }

        const lt = new LanguageTrans({
            language: req.body.language,
            flag: req.body.flag,
            fields:JSON.parse(req.body.fields)
        });

        const newlt = await lt.save();
        res.status(201).json({langtrans:newlt,message:"Language Translation created",status: "OK"});
    } catch (err) {
        res.status(400).json({ message: err,status:"Error" });
    }
};


exports.list_all_langtrans = async (req, res) => {
    try{
        LanguageTrans.find({},{language:1,flag:1}).
        exec(async function (err, langtrans) {
            await res.status(201).json({langtrans:langtrans,message:"",status:"OK"});
        });
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};
exports.save_langtrans = async (req,res) =>{
    try{
        LanguageTrans.find().
        exec(async function (err, langtrans) {
            let zip = new AdmZip();
            zip.addFile('LanguageTrans.json', Buffer.from(JSON.stringify(langtrans,null,4), "utf8"));
            const fpath = path.join(API.root,API.uploads_imgdir,'langtrans')
            if(fs.existsSync(fpath)){
                zip.addLocalFolder(fpath);        
            }
            zip.writeZip(path.join(API.root,API.template_dir,'LanguageTrans.zip'))
            await res.status(201).json({message:"Translation file saved",status:"OK"});
        });
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};
exports.load_langtrans = async (req,res) =>{
   try{
        await LanguageTrans.deleteMany({});
        let zip = new AdmZip(path.join(API.root,API.template_dir,'LanguageTrans.zip'));
        const jinput = JSON.parse(zip.readAsText('LanguageTrans.json'));
        await json2mango(jinput, LanguageTrans)
        zip.deleteFile('LanguageTrans.json');
        const fpath = path.join(API.uploads_imgdir,'langtrans');
        if(!fs.existsSync(fpath)) fs.mkdirSync(fpath);
        zip.extractAllTo(fpath,true);
    
        await res.status(201).json({message:"Translation file loaded",status:"OK"});
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};
exports.download_langtrans = async (req,res) =>{
    try{
        LanguageTrans.find().
        exec(async function (err, langtrans) {
            var zip = new AdmZip();
            zip.addFile("LanguageTrans.json", Buffer.from(JSON.stringify(langtrans), "utf8"));
            const fpath = path.join(API.root,API.uploads_imgdir,'langtrans')
            if(fs.existsSync(fpath)){
                zip.addLocalFolder(fpath);        
            }
            res.set("Content-Type", "application/octet-stream");
            res.set("Content-Disposition", "attachment;filename=LanguageTrans.zip");
            await res.send(zip.toBuffer());
        });
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};
exports.upload_langtrans = async (req,res) => {
   try{
        if (!req.files || Object.keys(req.files).length === 0) {
            return res.status(400).json({ message: "No file provided",status:"Error"});
        }
        await LanguageTrans.deleteMany();
        let zip =  new AdmZip(req.files.templatefile.data);

        const jinput = JSON.parse(zip.readAsText('LanguageTrans.json'))
        await json2mango(jinput, LanguageTrans)

        zip.deleteFile('LanguageTrans.json');
        const fpath = path.join(API.uploads_imgdir,'langtrans');
        if(!fs.existsSync(fpath)) fs.mkdirSync(fpath);
        zip.extractAllTo(fpath,true);


        await res.status(201).json({message:"Translation file uploaded",status:"OK"});
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};

exports.delete_a_langtrans = async (req, res) => {
    try {
        await LanguageTrans.findById(req.params.id).
        exec(async function (err, lang) {
            const fpath = path.join(API.root,API.uploads_imgdir,'langtrans',lang.language)
            if(fs.existsSync(fpath)){
                fs.rmSync(fpath,  {recursive: true});
            }
        });
        await LanguageTrans.deleteOne({ _id: req.params.id});
        res.json({
            id: req.params.id,
            message: 'Language Translation successfully deleted',
            status :"OK"
        });
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};

exports.update_a_langtrans = async (req, res) => {
    let fields = JSON.parse(req.body.fields);
    let urls=[];

        fields.forEach(val=>{
            if(val.text){
                    urls.push(...FindUrlinHtml(val.text,null,true));
            }
        })
        let listfile = getImgpathLangTrans(req.body.language);

        const file2rm = _.difference(listfile,urls);
        file2rm.forEach(fpath => {
            fs.rmSync(path.join(API.uploads_imgdir,'langtrans',fpath));
        })
   const filemissing = _.difference(urls,listfile);

   const validfiles={removed:file2rm.length,missing:filemissing.length}


    LanguageTrans.replaceOne(
        { _id: req.params.id },
        {
            language: req.body.language,
            flag: req.body.flag,
            fields:fields
        },
        (err) => {
            if (err) {res.status(400).json({ message: err,status:"Error"});}
            else{res.status(201).json({ id: req.params.id,message:"Language Translation modified",status: "OK"});}
        }
    );

};
exports.update_surveyglobaloptions = async (req, res) => {
    try {
        const check = await GlobalOptions.checkExistingField('_id', 0);
        if (check) {
            GlobalOptions.replaceOne(
                {_id: 0},
           req.body,
                (err) => {
                    if (err) {
                        res.status(400).json({message: err, status: "Error"});
                    } else {
                        res.status(201).json({message: "Survey global options updated", status: "OK"});
                    }
                });
        }
                else {
            let globalopt = new GlobalOptions(req.body);
            globalopt._id=0;
            await globalopt.save();
            res.status(201).json({message: "Survey global options created", status: "OK"});
        }
    } catch (err) {
        res.status(400).json({ message: err,status:"Error" });
    }

}
exports.get_surveyglobaloptions = async (req, res) => {
        try {
    const check = await GlobalOptions.checkExistingField('_id', 0);
            if(check) {

                const opt = await GlobalOptions.findById(0);
                await res.status(201).json({options: opt, message: "", status: "OK"});
            }
            else {
                await res.status(201).json({options: {}, message: "", status: "OK"});
            }
        } catch (err) {
            res.status(400).json({ message: err,status:"Error"});
        }
};

exports.getLangtrans = async (req, res) => {
    try {
        const lt = await LanguageTrans.findOne({ language: req.params.language });

        await res.status(201).json({langtrans:lt,message:"",status:"OK"});
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};


//img upload

exports.upload_img = async (req,res) => {
    try{
        if (!req.files || Object.keys(req.files).length === 0) {
            return res.status(201).json({ error:{message: "No file provided"}});
        }
        const file = req.files[Object.keys(req.files)[0]];
        let filename="";
        if(file.name){
            filename=file.name;
        }else if(mime.extension(file.mimetype)){
            filename=randomValueHex(15)+"."+mime.extension(file.mimetype);
        } else {
            return res.status(201).json({ error:{message: "filename impossible to determine"}});
        }
        if((!req.params.surveyid)||(!req.params.objname)||(!req.params.lang)){
            return res.status(201).json({ error:{message: "input parameter missing"}});
        }

        let dir = path.join(req.params.lang,req.params.objname);
        if(req.params.subobjname){
            dir = path.join(dir,req.params.subobjname);
        }
        let fpath = path.join(API.uploads_imgdir,req.params.surveyid,dir);
        if(!fs.existsSync(fpath)) fs.mkdirSync(fpath, { recursive: true });
        fpath= path.join(fpath,filename);
        file.mv(fpath);
        return res.status(201).json({url:API.URL+"surveys/survey/"+req.params.surveyid+"/picture/"+dir+"/"+filename});
    } catch (err) {
         return res.status(201).json({ error:{message: err}});
    }
};


exports.get_img = async (req,res) =>{
   try{
        let options = {
            root: API.uploads_imgdir,
            dotfiles: 'deny',
            headers: {
              'x-timestamp': Date.now(),
              'x-sent': true
            }
          }
          let dir = path.join(req.params.lang,req.params.objname);
        if(req.params.subobjname){
            dir = path.join(dir,req.params.subobjname);
        }
          let filename = path.join(req.params.surveyid,dir,req.params.filename);
          res.sendFile(filename, options);
    } catch (err) {
         res.status(400).json({ message: err,status:"Error"});
     }
};

exports.upload_img_langtrans = async (req,res) => {
    try{
        if (!req.files || Object.keys(req.files).length === 0) {
            return res.status(201).json({ error:{message: "No file provided"}});
        }
        const file = req.files[Object.keys(req.files)[0]];
        let filename="";
        if(file.name){
            filename=file.name;
        }else if(mime.extension(file.mimetype)){
            filename=randomValueHex(15)+"."+mime.extension(file.mimetype);
        } else {
            return res.status(201).json({ error:{message: "filename impossible to determine"}});
        }
        if((!req.params.id)||(!req.params.lang)){
            return res.status(201).json({ error:{message: "input parameter missing"}});
        }

        let dir = path.join(req.params.lang);

        let fpath = path.join(API.uploads_imgdir,'langtrans',dir,req.params.id);
        if(!fs.existsSync(fpath)) fs.mkdirSync(fpath, { recursive: true });
        fpath= path.join(fpath,filename);
        file.mv(fpath);
        return res.status(201).json({url:API.URL+"surveys/langtrans/picture/"+dir+"/"+req.params.id+"/"+filename});
    } catch (err) {
         return res.status(201).json({ error:{message: err}});
    }
};


exports.get_img_langtrans = async (req,res) =>{
   try{
        let options = {
            root: path.join(API.uploads_imgdir,'langtrans'),
            dotfiles: 'deny',
            headers: {
              'x-timestamp': Date.now(),
              'x-sent': true
            }
          }
          let dir = path.join(req.params.lang);
          let filename = path.join(dir,req.params.id,req.params.filename);
          res.sendFile(filename, options);
    } catch (err) {
         res.status(400).json({ message: err,status:"Error"});
     }
};


//client side

exports.getSurveykey = async (req, res) => {
    try {
        const key=req.params.key;
        const proj={}
        proj[key]=1;
        let survey = await Survey.findById(req.userData.survey_id,proj);
        survey=survey[key];
        if (key === "welcome"){
            survey = survey.find(o => o.language === req.userData.language)
        } else if (key === "presurvey" || key === "postsurvey"){
             tmp=[]
             for(let item of survey){
                 if(item.enable) {
                     item.contents = item.contents.find(o => o.language === req.userData.language)
                     delete item.description
                     delete item.comments
                     tmp.push(item);
                 }
             }
             survey = tmp;

        } else if (key === "rational"){
            let tmp=[]
            survey.general.conclusiontext.content = survey.general.conclusiontext.content.find(o => o.language === req.userData.language)

            survey.alternatives.content = survey.alternatives.content.find(o => o.language === req.userData.language)
            survey.objectives.content = survey.objectives.content.find(o => o.language === req.userData.language)

            for(let item of survey.alternatives.items){
                if(item.enable) {
                    item.elements.contents = item.elements.contents.find(o => o.language === req.userData.language)
                    delete item.description
                    delete item.comments
                    tmp.push(item);
                }
            }
            survey.alternatives.items = tmp;
            let tmp2=[]
            for(let item of survey.objectives.items){
                if(item.enable) {
                    item.elements.contents = item.elements.contents.find(o => o.language === req.userData.language)
                    delete item.description
                    delete item.comments
                    if(item.type==='category'&&item.children.length>0){
                        let stmp=[]
                        for(let item2 of item.children) {
                            if (item.enable) {
                                item2.elements.contents = item2.elements.contents.find(o => o.language === req.userData.language)
                                delete item2.description
                                delete item2.comments
                                stmp.push(item2)
                            }
                            item.children=stmp
                        }
                    }
                    tmp2.push(item);
                }
            }
            survey.objectives.items = tmp2;

        }
        if (survey){
            await res.status(201).json({payload:survey,message:key,status:"OK"});
        } else {
            res.status(400).json({ message: "key doesn't exist",status:"Error"});
        }

    } catch (err) {
         res.status(400).json({ message: err,status:"Error"});
    }
};

exports.getSurveyInfo= async (req, res) => {
    try {
        const survey = await Survey.findById(req.userData.survey_id);
        const poc = await survey.poc
        if (!poc) {
            return res
                .status(400)
                .json({ message: "Contact user is not defined" ,status: "Error"});
        }
        let surveygeneral = survey.general;
        let  languages = []

        for(let i in surveygeneral.languages) {
                const lang = surveygeneral.languages[i]
                const lt =  await LanguageTrans.findOne({ language: lang },{flag:1});
                languages.push({id:lang,flag:lt.flag})
            }
        // let poc = surveygeneral.poc
        // if (poc.useglobal){
        //     const opt = await GlobalOptions.findById(0,{poc:1});
        //     poc = opt.poc;
        // }
        const info = {poc:poc,languages:languages,externalurl:surveygeneral.externalurl}
        await res.status(201).json({payload:info,message:"info",status:"OK"});
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};

exports.postLangtrans = async (req, res) => {
    try {
        //Generate a new token that will contain the selected language
        const token = await Token.findById(req.userData.id);
        const authtoken = await token.generateAuthToken({language:req.params.language});
        const lt = await LanguageTrans.findOne({ language: req.params.language });
        let list={}
        for(let i in lt.fields){
            const element = lt.fields[i];
            list[element.field]=element.text;
        }
        await res.status(201).json({langtrans:list,token:authtoken,message:"token updated",status:"OK"});
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};

