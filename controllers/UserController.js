const mongoose = require('mongoose');
const AdminUser =  mongoose.model('AdminUser');

exports.registerNewUser = async (req, res) => {
   try {
       req.body.username=req.body.username.toLowerCase();
       const checkAdminUser = await AdminUser.checkExistingField('username', req.body.username);
        if (checkAdminUser) {
            return res.status(201).json({
                message: "username already used",
                status: "Error"
            });
        }
        const user = new AdminUser(req.body);
        const newuser = await user.save();
        const token = await user.generateAuthToken();
        res.status(201).json({token:token,user:newuser,message:"User created",status: "OK"});
   } catch (err) {
        res.status(400).json({ message: err,status:"Error" });
   }
};
exports.loginUser = async (req, res) => {
    try {
        const username = req.body.username.toLowerCase();
        const password = req.body.password;
        const user = await AdminUser.findOne({ username:username });
        if (!user) {
            return res
                .status(201)
                .json({ message: "Login failed! Check authentication credentials" ,status: "Error"});
        }
        const matchpassword = await user.comparePassword(password);
        if (!matchpassword){
            return res
                .status(201)
                .json({ message: "Login failed! Check authentication credentials" ,status: "Error"});
        }
        if(!user.enable){
            return res
                .status(201)
                .json({ message: "Login disabled!" ,status: "Error"});
        }

        const token = await user.generateAuthToken();
        res.status(201).json({ token:token, message : "Succeeded Login", status: "OK"});
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};
exports.update_user = async (req, res) => {
    try {
        const user = await AdminUser.findOne({ _id: req.userData.id });
        for (key in req.body) {
            user[key] = req.body[key];
        }
        await user.save();
        res.json({message:"User modified",status: "OK"});
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};


exports.getUserDetails = async (req, res) => {
    try {

        await res.status(201).json({user:req.userData,message:"",status:"OK"});
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};



exports.list_all_users = async (req, res) => {
    try {
        AdminUser.find({},).
        exec(async function (err, users) {
            await res.status(201).json({users:users,message:"",status:"OK"});
        });
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};

exports.get_a_user = async (req, res) => {
    try {
        const user = await AdminUser.findOne({ _id: req.params.userId });
        await res.status(201).json({user:user,message:"",status:"OK"});
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};

exports.update_a_user = async (req, res) => {
    try {
        const user = await AdminUser.findOne({ _id: req.params.userId });
        for (key in req.body) {
            user[key] = req.body[key];
        }
        await user.save();
        res.json({ id: req.params.userId,message:"User modified",status: "OK"});
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};

exports.delete_a_user = (req, res) => {
    try {
        AdminUser.deleteOne({_id: req.params.userId}, err => {
            res.json({
                id: req.params.userId,
                message: 'User successfully deleted',
                status: "OK"
            });
        });
    } catch (err) {
        res.status(400).json({message: err, status: "Error"});
    }
};
exports.is_admin_exist = async (req, res) => {
    const number = await AdminUser.countDocuments();
    res.json({
        adminpresent: number>0,
        message: '',
        status: "OK"
    });
};
exports.create_firstadmin = async (req, res) => {
    if(await AdminUser.countDocuments() > 0) {
        res.json({
            message: 'Operation not possible, admin user are already in the database',
            status: "Error"
        });
    }else {
        const user = new AdminUser(req.body);
        const newuser = await user.save();
        const token = await user.generateAuthToken();
        res.status(201).json({token:token,user:newuser,message:"User created",status: "OK"});
    }
};
