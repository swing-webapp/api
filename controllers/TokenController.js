const mongoose = require('mongoose');
const Token =  mongoose.model('Token');
const Survey =  mongoose.model('Survey');
const GlobalOptions = mongoose.model('SurveyGlobalOptions');

exports.registerNewToken = async (req, res) => {
   try {
       const checkToken = await Token.checkExistingField('token', req.body.token);
        if (checkToken) {
            return res.status(201).json({
                message: "token already used",
                status: "Error"
            });
        }
        let token;
        if(req.body.advanced){
            delete req.body.advanced
             token = new Token(req.body)
        } else {
            delete req.body.advanced
             token = new Token({
                token: req.body.token,
                survey_id: req.body.survey_id,
                enable: req.body.enable,
                status:"new",
                access:false,
                comments:req.body.comments,
                test:req.body.test,
            });
        }

        const newtoken = await token.save();
        res.status(201).json({token:newtoken,message:"Token created",status: "OK"});
   } catch (err) {
        res.status(400).json({ message: err,status:"Error" });
   }
};

exports.update_token = async (req, res) => {
    try {
        const token = await Token.findOne({ _id: req.userData.id });
        for (key in req.body) {
            token[key] = req.body[key];
        }
        await token.save();
        res.json({message:"Token modified",status: "OK"});
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};




exports.list_all_tokens = async (req, res) => { //TODO : add number of attached token and number of finished token
    try {
        Token.find({test:false},).
        exec(async function (err, tokens) {
            await res.status(201).json({tokens:tokens,message:"",status:"OK"});
        });
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};

exports.list_survey_tokens = async (req,res)=>{
    try {
        Token.find({survey_id:req.params.survey_id,test:false},).
        exec(async function (err, tokens) {
            await res.status(201).json({tokens:tokens,message:"",status:"OK"});
        });
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};
exports.test_survey_token = async (req,res)=>{
    try {
        Token.findOne({survey_id:req.params.survey_id,test:true},).
        exec(async function (err, tokens) {
            await res.status(201).json({tokens:tokens,message:"",status:"OK"});
        });
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};

exports.get_a_token = async (req, res) => {
    try {
        const token = await Token.findById(req.params.id);
        await res.status(201).json({token:token,message:"",status:"OK"});
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};

exports.update_a_token = async (req, res) => {
    try {
        const token = await Token.findOne({ _id: req.params.id });
        let oldstatus=token.status;
        for (key in req.body) {
            token[key] = req.body[key];
        }
        if(token.status==='new'&&oldstatus!=='new'){
            token.answers=null
            token.submitedAt=null
        }
        token.admin=false;
        await token.save();
        res.json({ id: req.params.id,message:"Token modified",status: "OK"});
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};

exports.delete_a_token = (req, res) => {
    try {
    Token.deleteOne({ _id: req.params.id }, err => {
        res.json({
            id: req.params.id,
            message: 'Token successfully deleted',
            status :"OK"
        });
    });
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};

exports.downJSON_survey_tokens = async (req,res)=>{
    try{
        const survey=await Survey.findById(req.params.survey_id)
        Token.find({survey_id:req.params.survey_id,test:false},).
        exec(async function (err, tokens) {
            tokens.forEach(o=>o.survey_id=survey.general.short_name)
            tokens.forEach(o=>{delete o.test; delete o.id})
            res.set("Content-Type", "application/octet-stream");
            res.set("Content-Disposition", "attachment;filename="+survey.general.short_name+"-token.json");
            await res.send(tokens);
        });
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};
function removeNonAsciiChars(str){
    if(typeof str === 'string'){
        str = str.replace(/[^\w\s]/gi, "_");
        str = str.replace(/(\r\n|\n|\r)/gm,"");
    }
    return str;
}
exports.downCSV_survey_tokens = async (req,res)=>{
    const { Parser,transforms: {  flatten }  } = require('json2csv');
    try{
        const survey=await Survey.findById(req.params.survey_id)
        Token.find({survey_id:req.params.survey_id,test:false},).
        exec(async function (err, tokens) {
            let tokensjson=await JSON.parse(JSON.stringify(tokens))
            tokensjson.forEach(o=>o.survey_id=survey.general.short_name)
            tokensjson.forEach(o=>{delete o.test; delete o.id})


            tokensjson.forEach(o=>{ //remove special characters in post and presurvey
                if (o.answers) {
                    if(o.answers.presurvey) Object.keys(o.answers.presurvey).forEach(function(el){o.answers.presurvey[el]=removeNonAsciiChars(o.answers.presurvey[el])});
                    if(o.answers.postsurvey) Object.keys(o.answers.postsurvey).forEach(function(el){o.answers.postsurvey[el]=removeNonAsciiChars(o.answers.postsurvey[el])});
                }
            });

            const transforms = [flatten({ paths: ['answers'] })];

            const parser = new Parser({transforms});
            try {
                const tokenscsv = await parser.parse(tokensjson);
                res.set("Content-Type", "application/octet-stream");
                res.set("Content-Disposition", "attachment;filename="+survey.general.short_name+"-token.csv");
                await res.send(tokenscsv);
            }catch (err) {
                res.status(400).json({ message: err,status:"Error"});
            }

        });
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};
// client

exports.loginToken = async (req, res) => {
    try {
        const token = await Token.findOne({ token:req.body.token });
        const gopt = await GlobalOptions.findById(0,{poc:1});
        if (!gopt) {
            return res
                .status(400)
                .json({ message: "Global user is not defined" ,status: "Error"});
        }
        if (!token) {
            return res
                .status(201)
                .json({ message: "This token does not exist" ,poc:gopt.poc,status: "Error"});
        }

        const survey = await Survey.findById(token.survey_id)
        let spoc= await survey.poc
        if(!token.enable){
            return res
                .status(201)
                .json({ message: "Token disabled!" ,poc:spoc,status: "Error"});
        }
        if(!survey.status && !token.test){
            return res
                .status(201)
                .json({ message: "Survey not online" ,poc:spoc,status: "Error"});
        }
        if(!survey.general.enable && !token.test){
            return res
                .status(201)
                .json({ message: "Survey disabled",poc:spoc ,status: "Error"});
        }

        if(token.status==="finished"){
            return res
                .status(201)
                .json({ message: "Status : finished" ,poc:spoc,status: "Error"});
        }
        const authtoken = await token.generateAuthToken();
        res.status(201).json({ token:authtoken, message : "Succeeded Login", status: "OK"});
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};
exports.accessToken = async (req, res) => {
    if(await Survey.isAccessToken(req.body.survey_id)){
        try{
            const survey = await Survey.findById(req.body.survey_id)
            let spoc= await survey.poc
            if (!spoc) {
                return res
                    .status(400)
                    .json({ message: "Global user is not defined" ,status: "Error"});
            }
            let token = await Token.findOne({ token:req.body.token });
            if(!survey.status && (!token || !token.test)){
                return res
                    .status(201)
                    .json({ message: "Survey not online" ,poc:spoc,status: "Error"});
            }
            if(!survey.general.enable && (!token ||!token.test)){
                return res
                    .status(201)
                    .json({ message: "Survey disabled" ,poc:spoc,status: "Error"});
            }
        if (!token) { //create new token if doesn't exist
            const newtoken = new Token({
                token: req.body.token,
                survey_id: req.body.survey_id,
                enable: true,
                access:true,
                status:"new",
                comments:"Created with API access token",
                test:false,
            });
            token = await newtoken.save();
        }
        if(!token.enable){
            return res
                .status(201)
                .json({ message: "Token disabled!" ,poc:survey.poc,status: "Error"});
        }
        const authtoken = await token.generateAuthToken();
        res.status(201).json({ token:authtoken, message : "Succeeded Login", status: "OK"});
        } catch(err){
            res.status(400).json({ message: err,status:"Error"});
        }
    } else {
        const gopt = await GlobalOptions.findById(0,{poc:1});
        res.status(400).json({ message: "Forbidden Operation",poc:gopt.poc,status:"Error"});

    }
}

exports.getTokenDetails = async (req, res) => {
    try {

        await res.status(201).json({user:req.userData,message:"",status:"OK"});
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};

exports.update_status = async (req, res) => {
    try {
        const token = await Token.findById(req.userData.id );
        if(!token.enable){
            return res
                .status(201)
                .json({ message: "Token disabled!" ,status: "Error"});
        }
        if(token.status==="finished"){
            return res
                .status(201)
                .json({ message: "Status : finished update impossible" ,status: "Error"});
        }
        if(token.test){
            return res
                .status(201)
                .json({ message: "Nothing saved : test token" ,status: "Error"});
        }
        token.status = req.body.status;
        if(token.status==="finished") token.submitedAt = Date.now();
        await token.save();
        res.status(201).json({message:"Token status modified",status: "OK"});
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};

exports.update_answers = async (req, res) => {
    try {
        const token = await Token.findById(req.userData.id);
        if(token.status==="finished"){
            return res
                .status(201)
                .json({ message: "Status : finished update impossible" ,status: "Error"});
        }
        if(!token.enable){
            return res
                .status(201)
                .json({ message: "Token disabled!" ,status: "Error"});
        }
        if(token.test){
            return res
                .status(201)
                .json({ message: "Nothing saved : test token" ,status: "Error"});
        }
        token.answers = req.body.answers;
        await token.save();
        res.status(201).json({message:"Token answers modified",status: "OK"});
    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};
exports.get_answers = async (req, res) => {
    try {
        const token = await Token.findById(req.userData.id);
        res.status(201).json({answers:token.answers,message:"",status: "OK"});

    } catch (err) {
        res.status(400).json({ message: err,status:"Error"});
    }
};
