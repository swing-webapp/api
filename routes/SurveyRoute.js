const Builder = require("../controllers/SurveyController");
const auth = require("../middleware/auth");
module.exports = app => {
    app.get("/surveys/drop", auth(true), Builder.dropSurvey);

    app.get("/surveys/all", auth(true), Builder.list_all_surveys);
    app.post("/surveys/create", auth(true), Builder.createNewSurvey);

    app.get("/surveys/survey/:id", auth(true), Builder.get_a_survey);
    app.get("/surveys/downsurvey/:id", auth(true), Builder.download_survey);
    app.post("/surveys/upload",auth(true),Builder.upload_survey)
    app.post("/surveys/clone",auth(true),Builder.clone_survey)

    app.delete("/surveys/survey/:id", auth(true), Builder.delete_a_survey);
    app.put("/surveys/survey/:id", auth(true), Builder.update_a_survey);
    app.put("/surveys/survey/enable/:id", auth(true), Builder.enable_a_survey);
    app.put("/surveys/survey/disable/:id", auth(true), Builder.disable_a_survey);


    app.get("/surveys/langdef", auth(true), Builder.list_all_langdef);
    app.post("/surveys/langdef", auth(true), Builder.createNewLangdef);
    app.delete("/surveys/langdef/:id", auth(true), Builder.delete_a_langdef);
    app.put("/surveys/langdef/:id", auth(true), Builder.update_a_langdef);
    app.get("/surveys/savelangdef", auth(true), Builder.save_langdef);
    app.get("/surveys/loadlangdef", auth(true), Builder.load_langdef);
    app.get("/surveys/downlangdef", auth(true), Builder.download_langdef);
    app.post("/surveys/uploadlangdef", auth(true), Builder.upload_langdef);


    app.get("/surveys/langtrans", auth(true), Builder.list_all_langtrans);
    app.get("/surveys/langtrans/:language", auth(), Builder.getLangtrans);
    app.post("/surveys/langtrans", auth(true), Builder.createNewLangtrans);
    app.delete("/surveys/langtrans/:id", auth(true), Builder.delete_a_langtrans);
    app.put("/surveys/langtrans/:id", auth(true), Builder.update_a_langtrans);
    app.get("/surveys/savelangtrans", auth(true), Builder.save_langtrans);
    app.get("/surveys/loadlangtrans", auth(true), Builder.load_langtrans);
    app.get("/surveys/downlangtrans", auth(true), Builder.download_langtrans);
    app.post("/surveys/uploadlangtrans", auth(true), Builder.upload_langtrans);


    app.post("/surveys/options", auth(true), Builder.update_surveyglobaloptions);
    app.get("/surveys/options", auth(true), Builder.get_surveyglobaloptions);


  // img  
    app.post("/surveys/survey/:surveyid/picture/:lang/:objname",auth(true),Builder.upload_img);
    app.post("/surveys/survey/:surveyid/picture/:lang/:objname/:subobjname",auth(true),Builder.upload_img);

    app.get("/surveys/survey/:surveyid/picture/:lang/:objname/:filename",Builder.get_img);
    app.get("/surveys/survey/:surveyid/picture/:lang/:objname/:subobjname/:filename",Builder.get_img);

    app.post("/surveys/langtrans/picture/:lang/:id", auth(true), Builder.upload_img_langtrans);
    
    app.get("/surveys/langtrans/picture/:lang/:id/:filename", Builder.get_img_langtrans);


// client side

    app.get("/surveys/me/get/:key", auth(), Builder.getSurveykey);
    app.get("/surveys/me/infos", auth(), Builder.getSurveyInfo);
    app.post("/surveys/me/languages/:language", auth(), Builder.postLangtrans);

}
