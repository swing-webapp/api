const Builder = require("../controllers/TokenController");
const auth = require("../middleware/auth");

module.exports = app => {

    app.get("/tokens/all", auth(true), Builder.list_all_tokens);
    app.post("/tokens/register", auth(true), Builder.registerNewToken);

    app.get("/tokens/survey/:survey_id", auth(true), Builder.list_survey_tokens);
    app.get("/tokens/survey/:survey_id/test", auth(true), Builder.test_survey_token);
    app.get("/tokens/downjson/:survey_id", auth(true), Builder.downJSON_survey_tokens);
    app.get("/tokens/downcsv/:survey_id", auth(true), Builder.downCSV_survey_tokens);

    app.get("/tokens/token/:id", auth(true), Builder.get_a_token);
    app.delete("/tokens/token/:id", auth(true), Builder.delete_a_token);
    app.put("/tokens/token/:id", auth(true), Builder.update_a_token);

    //Client side
    app.post("/tokens/login", Builder.loginToken);
    app.post("/tokens/access", Builder.accessToken);

    app.get("/token/me", auth(), Builder.getTokenDetails);
    app.put("/token/me/status", auth(), Builder.update_status)
    app.put("/token/me/answers", auth(), Builder.update_answers)
    app.get("/token/me/answers", auth(), Builder.get_answers)


};
