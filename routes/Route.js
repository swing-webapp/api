module.exports = app => {
    const VERSION = require('../package.json').version

    app.get('/',async (req, res) => {
        return res
            .status(400)
            .json({ message: "API EAWAG Version "+VERSION ,status: "OK"});
    })

};
