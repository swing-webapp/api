const userBuilder = require("../controllers/UserController");
const auth = require("../middleware/auth");

module.exports = app => {
    app.post("/adminusers/login", userBuilder.loginUser);

    app.get("/adminusers/all", auth(true), userBuilder.list_all_users);
    app.post("/adminusers/register", auth(true), userBuilder.registerNewUser);

    app.get("/adminusers/user/:userId", auth(true), userBuilder.get_a_user);
    app.delete("/adminusers/user/:userId", auth(true), userBuilder.delete_a_user);
    app.put("/adminusers/user/:userId", auth(true), userBuilder.update_a_user);

    app.get("/adminusers/me", auth(true), userBuilder.getUserDetails);
    app.put("/adminusers/me", auth(true), userBuilder.update_user);

    app.get("/adminusers/exist", userBuilder.is_admin_exist);
    app.post("/adminusers/createfirst", userBuilder.create_firstadmin);

};
